Manual Técnico del Proyecto

Prueba Técnica Symfony2

**Pasos para implemtar el proyecto

1. Importar Base de datos (pagomio). Se encuentra en la carpeta DataBase.

2. Por consola, pararse sobre el proyecto (Carpeta pagomio) y ejecutar el siguiente comando:

composer update

y luego: 

php app/console server:run


URL Website** - http://127.0.0.1:8000/
Credenciales Acceso Administrador -

user: admin@pagomio.com
password: Pagomio.2016
url: /login

3. Problema:
Pagomío requiere crear una nueva funcionalidad para su plataforma transaccional. Esta está enfocada en permitir  que sus administradores tengan la posibilidad de asignar los métodos de pagos disponibles (Visa, Mastercad, American Express) a sus marcas existentes (Marca1,Marca2), además de poder definir la comisión que tendrá cada marca para cada método de pago asignado.

4. Requisitos:
Usar  Symfony2 ultima version. 
Crear Bundle PaymentBundle
Crear entidades necesarias para solucionar el problema.
Crear CRUD para la entidad marca (Listar,Agregar, Editar, Eliminar). 
Usar Bootstrap para las interfaces gráficas. 



Descripción General del Proyecto:
Proyecto de prueba técnica donde los administradores pueden crear marcas, metodos de pago y asignarle una comisión a cada metodo de pago creado por marca. Al ingresar al sistema ya estaran generados los metodos de pago (Visa, Mastercad, American Express). Al crear una nueva marca e ir a Editarla aparecera una opción llamada (Manejar metodos de pago), en la cual podremos ingresar el metodo de pago y ademas una comisión que se guardara solo para ese marca.


Tipo de Aplicación - Administrador web desarrollado a la medida
Lenguajes de Desarrollo - PHP
Frameworks de Desarrollo - Symfony 2.8
Base de datos Mysql