<?php

namespace PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment_methods
 *
 * @ORM\Table(name="payment_methods")
 * @ORM\Entity(repositoryClass="PaymentBundle\Repository\Payment_methodsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Payment_methods
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Payment_methods
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Payment_methods
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Payment_methods
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Payment_methods
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }


    public function __toString() {
        return $this->name;
    }


    /**
     * @ORM\OneToMany(targetEntity="Brand_payment", mappedBy="Payment_methods", cascade={"persist"})
     */
    private $brand_payment;
 
    public function __construct()
    {
        $this->brand_payment = new \Doctrine\Common\Collections\ArrayCollection();
    }
 
    public function addBrandPayment(Brand_payment $brand_payment)
    {
        $brand_payment->setPaymentMethods($this);
        $this->brand_payment[] = $brand_payment;
        return $this;
    }
 
    public function removeBrandPayment(Brand_payment $brand_payment)
    {
        $this->brand_payment->removeElement($brand_payment);
    }

    /**
     * Get brand_payment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBrandPayment()
    {
        return $this->brand_payment;
    }


    /**
     * @ORM\PrePersist
     */
     public function setCreatedAtValue()
    {
       $this->createdAt=new \DateTime();
    }

     /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
     public function setUpdateAtValue()
    {
       $this->updatedAt=new \DateTime();
    }
}
