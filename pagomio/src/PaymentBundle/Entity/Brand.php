<?php

namespace PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brand
 *
 * @ORM\Table(name="brand")
 * @ORM\Entity(repositoryClass="PaymentBundle\Repository\BrandRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Brand
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email_contact", type="string", length=100)
     */
    private $emailContact;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_contact", type="string", length=50)
     */
    private $phoneContact;

    /**
     * @var string
     *
     * @ORM\Column(name="direction", type="string", length=100)
     */
    private $direction;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set emailContact
     *
     * @param string $emailContact
     * @return Brand
     */
    public function setEmailContact($emailContact)
    {
        $this->emailContact = $emailContact;

        return $this;
    }

    /**
     * Get emailContact
     *
     * @return string 
     */
    public function getEmailContact()
    {
        return $this->emailContact;
    }

    /**
     * Set phoneContact
     *
     * @param string $phoneContact
     * @return Brand
     */
    public function setPhoneContact($phoneContact)
    {
        $this->phoneContact = $phoneContact;

        return $this;
    }

    /**
     * Get phoneContact
     *
     * @return string 
     */
    public function getPhoneContact()
    {
        return $this->phoneContact;
    }


    /**
     * Set direction
     *
     * @param string $direction
     * @return Brand
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return string 
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Brand
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Brand
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Brand
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

   
    public function __toString() {
        return $this->name;
    }


    /**
     * @ORM\OneToMany(targetEntity="Brand_payment", mappedBy="brand", cascade={"persist"})
     */
    private $brand_payment;

    public function __construct()
    {
        $this->brand_payment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addBrandPayment(Brand_payment $brand_payment)
    {
        $brand_payment->setBrand($this);
        $this->brand_payment[] = $brand_payment;
        return $this;
    }

    public function removeBrandPayment(Brand_payment $brand_payment)
    {
        $this->brand_payment->removeElement($brand_payment);
    }


    /**
     * Get brand_payment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBrandPayment()
    {
        return $this->brand_payment;
    }


    /**
     * @ORM\PrePersist
     */
     public function setCreatedAtValue()
    {
       $this->createdAt=new \DateTime();
    }

     /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
     public function setUpdateAtValue()
    {
       $this->updatedAt=new \DateTime();
    }

}
