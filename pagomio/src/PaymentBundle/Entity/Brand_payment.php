<?php

namespace PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brand_payment
 *
 * @ORM\Table(name="brand_payment")
 * @ORM\Entity(repositoryClass="PaymentBundle\Repository\Brand_paymentRepository")
 */
class Brand_payment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * 
     * @ORM\Column(name="id_brand", type="integer")
     */
    private $idBrand;

    /**
     * @var int
     *
     * @ORM\Column(name="id_payment", type="integer")
     */
    private $idPayment;

    /**
     * @var string
     *
     * @ORM\Column(name="commission", type="string", length=255)
     */
    private $commission;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idBrand
     *
     * @param integer $idBrand
     * @return Brand_payment
     */
    public function setIdBrand($idBrand)
    {
        $this->idBrand = $idBrand;

        return $this;
    }

    /**
     * Get idBrand
     *
     * @return integer 
     */
    public function getIdBrand()
    {
        return $this->idBrand;
    }

    /**
     * Set idPayment
     *
     * @param integer $idPayment
     * @return Brand_payment
     */
    public function setIdPayment($idPayment)
    {
        $this->idPayment = $idPayment;

        return $this;
    }

    /**
     * Get idPayment
     *
     * @return integer 
     */
    public function getIdPayment()
    {
        return $this->idPayment;
    }

    /**
     * Set commission
     *
     * @param string $commission
     * @return Brand_payment
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return string 
     */
    public function getCommission()
    {
        return $this->commission;
    }


     /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="brand_payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_brand", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $brand;
 
    /**
     * @ORM\ManyToOne(targetEntity="Payment_methods", inversedBy="brand_payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_payment", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $payment_methods;
 


    /**
     * Set brand
     *
     * @param \PaymentBundle\Entity\Brand $brand
     * @return Brand_payment
     */
    public function setBrand(\PaymentBundle\Entity\Brand $brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \PaymentBundle\Entity\Brand 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set payment_methods
     *
     * @param \PaymentBundle\Entity\Payment_methods $paymentMethods
     * @return Brand_payment
     */
    public function setPaymentMethods(\PaymentBundle\Entity\Payment_methods $paymentMethods)
    {
        $this->payment_methods = $paymentMethods;

        return $this;
    }

    /**
     * Get payment_methods
     *
     * @return \PaymentBundle\Entity\Payment_methods 
     */
    public function getPaymentMethods()
    {
        return $this->payment_methods;
    }
}
