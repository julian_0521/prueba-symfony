<?php

namespace PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PaymentBundle\Entity\Brand_payment;
use PaymentBundle\Entity\Brand;
use PaymentBundle\Form\Brand_paymentType;

/**
 * Brand_payment controller.
 *
 * @Route("/brand_payment")
 */
class Brand_paymentController extends Controller
{
    /**
     * Lists all Brand_payment entities.
     *
     * @Route("/", name="brand_payment_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {

        $session = $request->getSession();
        if (!$session->has('id')) {
             $this->get('session')->getFlashBag()->add(
                            'message',
                            'Debes estar Logueado para ingresar al sistema'
         );
        return $this->redirect($this->generateUrl('payment_login'));
        }
        $em = $this->getDoctrine()->getManager();

        $brand_payments = $em->getRepository('PaymentBundle:Brand_payment')->findAll();

        return $this->render('brand_payment/index.html.twig', array(
            'brand_payments' => $brand_payments,
        ));
    }

    /**
     * Creates a new Brand_payment entity.
     *
     * @Route("/new/{brand}", name="brand_payment_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Brand $brand)
    {
        $brand_payment = new Brand_payment();
        $brand_payment->setBrand($brand);
        $form = $this->createForm('PaymentBundle\Form\Brand_paymentType', $brand_payment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $payment = $brand_payment->getPaymentMethods();
            $em = $this->getDoctrine()->getManager();
            //$query = $em
            $query = $em->createQuery(
                    'SELECT COUNT(bp.id) 
                     FROM PaymentBundle\Entity\Brand_payment bp
                     WHERE bp.idBrand = ' . $brand->getId() . ' 
                     AND bp.idPayment = ' . $payment->getId()
                );
            if($query->getSingleScalarResult() == 0)
            {
                $em->persist($brand_payment);
                $em->flush();

                return $this->redirectToRoute('brand_payment_show', array('id' => $brand_payment->getId()));
            }
            else
            {
                $this->get('session')->getFlashBag()->add(
                    'message',
                    'No puedes asignar dos veces el mismo metodo de pago'
                );
                return $this->render('brand_payment/new.html.twig', array(
                    'brand_payment' => $brand_payment,
                      'brand' => $brand,
                    'form' => $form->createView(),
                ));
            }
        }

        return $this->render('brand_payment/new.html.twig', array(
            'brand_payment' => $brand_payment,
            'brand' => $brand,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a Brand_payment entity.
     *
     * @Route("/{id}", name="brand_payment_show")
     * @Method("GET")
     */
    public function showAction(Brand_payment $brand_payment)
    {
        $deleteForm = $this->createDeleteForm($brand_payment);

        return $this->render('brand_payment/show.html.twig', array(
            'brand_payment' => $brand_payment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Brand_payment entity.
     *
     * @Route("/{id}/edit", name="brand_payment_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Brand_payment $brand_payment)
    {
        $deleteForm = $this->createDeleteForm($brand_payment);
        $editForm = $this->createForm('PaymentBundle\Form\Brand_paymentType', $brand_payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($brand_payment);
            $em->flush();

            return $this->redirectToRoute('brand_payment_edit', array('id' => $brand_payment->getId()));
        }

        return $this->render('brand_payment/edit.html.twig', array(
            'brand_payment' => $brand_payment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Brand_payment entity.
     *
     * @Route("/{id}", name="brand_payment_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Brand_payment $brand_payment)
    {
        $form = $this->createDeleteForm($brand_payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($brand_payment);
            $em->flush();
        }

        return $this->redirectToRoute('brand_payment_index');
    }

    /**
     * Creates a form to delete a Brand_payment entity.
     *
     * @param Brand_payment $brand_payment The Brand_payment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Brand_payment $brand_payment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('brand_payment_delete', array('id' => $brand_payment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
