<?php

namespace PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PaymentBundle\Entity\Brand;
use PaymentBundle\Entity\Brand_payment;
use PaymentBundle\Form\BrandType;

/**
 * Brand controller.
 *
 * @Route("/brand")
 */
class BrandController extends Controller
{
    /**
     * Lists all Brand entities.
     *
     * @Route("/", name="brand_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
         $session = $request->getSession();
        if (!$session->has('id')) {
             $this->get('session')->getFlashBag()->add(
                            'message',
                            'Debes estar Logueado para ingresar al sistema'
             );
            return $this->redirect($this->generateUrl('payment_login'));
            }

        $em = $this->getDoctrine()->getManager();

        $brands = $em->getRepository('PaymentBundle:Brand')->findAll();

        return $this->render('brand/index.html.twig', array(
            'brands' => $brands,
        ));
    }

    /**
     * Creates a new Brand entity.
     *
     * @Route("/new", name="brand_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $brand = new Brand();
        $form = $this->createForm('PaymentBundle\Form\BrandType', $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($brand);
            $em->flush();
            
            return $this->redirectToRoute('brand_show', array('id' => $brand->getId()));
        }

        return $this->render('brand/new.html.twig', array(
            'brand' => $brand,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Brand entity.
     *
     * @Route("/{id}", name="brand_show")
     * @Method("GET")
     */
    public function showAction(Brand $brand)
    {
        $deleteForm = $this->createDeleteForm($brand);
        $em = $this->getDoctrine()->getManager();
        $brandPayment = $em->getRepository('PaymentBundle:Brand_payment')
                        ->findBy(['idBrand' => $brand->getId()]);

        return $this->render('brand/show.html.twig', array(
            'brand' => $brand,
            'brand_payment' => $brandPayment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Brand entity.
     *
     * @Route("/{id}/edit", name="brand_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Brand $brand)
    {   
        $deleteForm = $this->createDeleteForm($brand);
        $editForm = $this->createForm('PaymentBundle\Form\BrandType', $brand);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($brand);
            $em->flush();


            // $em = $this->getDoctrine()->getManager();
            // $product = $em->getRepository('AppBundle:Product')->find($productId);

            // if (!$product) {
            //     throw $this->createNotFoundException(
            //         'No product found for id '.$productId
            //     );
            // }

            // $product->setName('New product name!');
            // $em->flush();


            return $this->redirectToRoute('brand_edit', array('id' => $brand->getId()));
        }

        return $this->render('brand/edit.html.twig', array(
            'brand' => $brand,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Brand entity.
     *
     * @Route("/{id}", name="brand_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Brand $brand)
    {
        $form = $this->createDeleteForm($brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($brand);
            $em->flush();
        }

        return $this->redirectToRoute('brand_index');
    }

    /**
     * Creates a form to delete a Brand entity.
     *
     * @param Brand $brand The Brand entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Brand $brand)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('brand_delete', array('id' => $brand->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    public function UpdatePaymentAction($brand,$payment)
    {
        die($brand . $payment);
    }



}
