<?php

namespace PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use PaymentBundle\Entity\Payment_methods;
use PaymentBundle\Form\Payment_methodsType;

/**
 * Payment_methods controller.
 *
 * @Route("/payment_methods")
 */
class Payment_methodsController extends Controller
{
    /**
     * Lists all Payment_methods entities.
     *
     * @Route("/", name="payment_methods_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('id')) {
             $this->get('session')->getFlashBag()->add(
                            'message',
                            'Debes estar Logueado para ingresar al sistema'
             );
            return $this->redirect($this->generateUrl('payment_login'));
            }

        $em = $this->getDoctrine()->getManager();

        $payment_methods = $em->getRepository('PaymentBundle:Payment_methods')->findAll();

        return $this->render('payment_methods/index.html.twig', array(
            'payment_methods' => $payment_methods,
        ));
    }

    /**
     * Creates a new Payment_methods entity.
     *
     * @Route("/new", name="payment_methods_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $payment_method = new Payment_methods();
        $form = $this->createForm('PaymentBundle\Form\Payment_methodsType', $payment_method);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment_method);
            $em->flush();

            return $this->redirectToRoute('payment_methods_show', array('id' => $payment_method->getId()));
        }

        return $this->render('payment_methods/new.html.twig', array(
            'payment_method' => $payment_method,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Payment_methods entity.
     *
     * @Route("/{id}", name="payment_methods_show")
     * @Method("GET")
     */
    public function showAction(Payment_methods $payment_method)
    {
        $deleteForm = $this->createDeleteForm($payment_method);

        return $this->render('payment_methods/show.html.twig', array(
            'payment_method' => $payment_method,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Payment_methods entity.
     *
     * @Route("/{id}/edit", name="payment_methods_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Payment_methods $payment_method)
    {
        $deleteForm = $this->createDeleteForm($payment_method);
        $editForm = $this->createForm('PaymentBundle\Form\Payment_methodsType', $payment_method);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment_method);
            $em->flush();

            return $this->redirectToRoute('payment_methods_edit', array('id' => $payment_method->getId()));
        }

        return $this->render('payment_methods/edit.html.twig', array(
            'payment_method' => $payment_method,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Payment_methods entity.
     *
     * @Route("/{id}", name="payment_methods_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Payment_methods $payment_method)
    {
        $form = $this->createDeleteForm($payment_method);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($payment_method);
            $em->flush();
        }

        return $this->redirectToRoute('payment_methods_index');
    }

    /**
     * Creates a form to delete a Payment_methods entity.
     *
     * @param Payment_methods $payment_method The Payment_methods entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Payment_methods $payment_method)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('payment_methods_delete', array('id' => $payment_method->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
