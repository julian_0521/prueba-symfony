<?php

namespace PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AutenticateController extends Controller
{
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        if ($session->has('id')) {
            return $this->render('PaymentBundle:User:index.html.twig' , array('user' => $user));
        } else
            {
                $this->get('session')->getFlashBag()->add(
                            'message',
                            'Debes estar Logueado para ingresar al sistema'

                        );

                        return $this->redirect($this->generateUrl('payment_login'));
            }

    }

    public function homeAction(Request $request)
    {
        $session = $request->getSession();
        if ($session->has('id')) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                    'SELECT COUNT(bp.id) 
                     FROM PaymentBundle\Entity\Brand bp'
            );

             $queryPayment = $em->createQuery(
                    'SELECT COUNT(bp.id) 
                     FROM PaymentBundle\Entity\Payment_methods bp'
            );

            //var_dump($query->getSingleScalarResult());exit;
            return $this->render('PaymentBundle:User:home.html.twig', array("brands" => $query->getSingleScalarResult() , "payment" => $queryPayment->getSingleScalarResult() ));
        } else
            {
                $this->get('session')->getFlashBag()->add(
                            'message',
                            'Debes estar Logueado para ingresar al sistema'

                        );

                        return $this->redirect($this->generateUrl('payment_login'));
            }

    }


    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        if ($session->has('id')) {
           $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                    'SELECT COUNT(bp.id) 
                     FROM PaymentBundle\Entity\Brand_payment bp'
            );

             $queryPayment = $em->createQuery(
                    'SELECT COUNT(bp.id) 
                     FROM PaymentBundle\Entity\Payment_methods bp'
            );
            //var_dump($query->getSingleScalarResult());exit;
            return $this->render('PaymentBundle:User:home.html.twig', array("brands" => $query->getSingleScalarResult() , "payment" => $queryPayment->getSingleScalarResult() ));
        } else
            {

                if($request->getMethod()=='POST')
                {
                    //die($request->getMethod());
                    $email=$request->get('_email');
                    $pass=$request->get('_password');

                    $password = sha1($pass);
                    
                   // echo $email . "<br/>" . $password . '</br>' . $hash; exit;

                    $user= $this->getDoctrine()->getRepository('PaymentBundle:User')->findOneBy(array("email"=>$email , "password" =>$password));

                    if($user)
                    {
                        $session = $request->getSession();
                        $session->set('id',$user->getId());
                        $session->set('first_name',$user->getFirstName());
                        $session->set('last_name',$user->getLastName());
                       // echo $session->get('last_name') ; exit;
                        return $this->redirect($this->generateUrl('payment_home'));
                    } else
                        {
                            $this->get('session')->getFlashBag()->add(
                                'message',
                                'Los datos ingresados no son validos'

                            );

                            return $this->redirect($this->generateUrl('payment_login'));
                        }
                }

                return $this->render('PaymentBundle:User:login.html.twig' );
            }
    }




    public function logoutAction(Request $request)
    {
        $session=$request->getSession();
        $session->clear();
         return $this->redirect($this->generateUrl('payment_login'));
    }

    public function addAction()
    {
        return $this->render('PaymentBundle:Default:contact.html.twig' );

    }


    public function editAction($id)
    {
        return $this->render('PaymentBundle:Default:contact.html.twig' , array('id' => $id));
    }


    public function viewAction($id)
    {
        $repository=$this->getDoctrine()->getRepository('PaymentBundle:User');

         // $user=$respository->find($id);
         $user=$repository->findOneById($id);

         return new Response('usuario: ' . $user->getUsername() . ' Email: ' . $user->getEmail() );
    }

     public function deletetAction($id)
    {
        return $this->render('PaymentBundle:Default:contact.html.twig' , array('id' => $id));
    }


}

