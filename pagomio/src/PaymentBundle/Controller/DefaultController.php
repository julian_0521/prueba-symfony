<?php

namespace PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PaymentBundle:Default:index.html.twig');
    }

     public function contactAction($name)
    {
        return $this->render('PaymentBundle:Default:contact.html.twig' , array('name' => $name));
    }

}
