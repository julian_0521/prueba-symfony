<?php

namespace PaymentBundle\Form\EventListener;
 
 
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
 
class ClearBrandPaymentsSubscriber implements EventSubscriberInterface
{
    private $om;
 
    /**
     * Constructor
     *
     * @param ObjectManager $om
     */
    function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }
 
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SUBMIT => 'postSubmit');
    }
 
    /**
     * Remove all certificates for one product
     *
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $this->om->persist($brand = $event->getForm()->getParent()->getData());
        $this->om->flush();
 
        $this->om->getRepository('PaymentBundle:Brand_payment')->clearManualSelection($brand, $event->getData());
    }
}