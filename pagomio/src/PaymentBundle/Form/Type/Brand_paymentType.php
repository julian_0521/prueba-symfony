<?php

namespace PaymentBundle\Form\Type;


use Doctrine\Common\Persistence\ObjectManager;
use PaymentBundle\Form\DataTransformer\ArrayToBrandPaymentTransform;
use PaymentBundle\Form\EventListener\ClearBrandPaymentsSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class Brand_paymentType extends AbstractType
{
    /*
     * @var ObjectManager $om
     *
    protected $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }*/

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new ArrayToBrandPaymentTransform($this->om));
        $builder->addEventSubscriber(new ClearBrandPaymentsSubscriber($this->om));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'choices' => 'PaymentBundle\Entity\Brand_payment'//$this->om->getRepository('PaymentBundle:Payment_methods')->getChoices(),
        ));

    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}