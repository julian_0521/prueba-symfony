<?php 

namespace PaymentBundle\Form\DataTransformer;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use PaymentBundle\Entity\Brand_payment;
//use CoreBundle\Doctrine\ORM\ClassificationRepository;
use PaymentBundle\Repository\Payment_methodsRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\Request;

class ArrayToBrandPaymentTransform implements DataTransformerInterface
{
    /**
     * @var ObjectManager $om
     */
    private $om;

    /**
     * Constructor
     *
     * @param ObjectManager $om
     */
    function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($values)
    {
        die($values);
        $payment_methods = array();
        if ($values) {
            foreach ($values as $value) {
                $payment_methods[] = $value->getPaymentMethods()->getId();
            }
        }
        return $payment_methods;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($selected)
    {
        $brand_payments = new ArrayCollection();

        if ($selected) {
            foreach($selected as $id) {
                $brand_payment = new Brand_payment();
                // Atributo extra de la relación. Imposible de guardar en una relación ManyToMany
                //$brand_payment->setAuto(false); 
                $brand_payment->setPaymentMethods($this->om->getRepository('PaymentBundle:Payment_methods')->find($id));
                $brand_payments[] = $brand_payment;
            }
        }

        return $brand_payments;
    }
}