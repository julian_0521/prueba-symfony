<?php

namespace PaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class Brand_paymentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('payment_methods', EntityType::class, array(
                'class' => 'PaymentBundle\Entity\Payment_methods',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('pay')
                        //->select('pay')
                        ->where("pay.isActive = 1")
                        ->orderBy('pay.name', 'ASC');
                    }))
            /*->add('brand', EntityType::class, array(
                'class' => 'PaymentBundle\Entity\Brand',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('brand')
                        ->where("brand.isActive = 1")
                        ->orderBy('brand.name', 'ASC');
                    }))*/
            ->add('commission');
    }

}